## terraform-gitlab

### Usage:

```
export TF_VAR_gitlab_token=<token>
export TF_VAR_enable_gitlab=true
export TF_VAR_repo=terraform-gitlab-test-repo
export TF_VAR_description="Terraform GitLab test"
export TF_VAR_private=false

terraform init
terraform plan
terraform apply
```
