variable "gitlab_token" {}
variable "repo" {}
variable "description" {}
variable "private" {}

variable "enable_gitlab" {
  default     = "false"
  type        = "string"
  description = "Only enable if using a Gitlab repo"
}

provider "gitlab" {
  token        = "${var.gitlab_token}"
}

resource "gitlab_project" "repo" {
  count              = "${var.enable_gitlab == "true" ? 1 : 0}"
  name               = "${var.repo}"
  description        = "${var.description}"
  visibility_level   = "${var.private == "true" ? "private" : "public" }"
}

output "git_clone_url" {
  value = "${gitlab_project.repo.*.ssh_url_to_repo}"
}
